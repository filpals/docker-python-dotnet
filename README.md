# Python-Dotnet Docker
Docker image based on Debian for Python as well as dotnet technology.

## Requirements
- Docker

## Getting started
Build and run.

```bash
# Build
$ docker build -t python-dotnet .

# Run
$ docker run -it python-dotnet
```

## How to use
Add build job in your `.gitlab-ci.yml` as below.

```yml
stages:
  - build

build:
 stage: build
 image: registry.gitlab.com/filpals/docker-python-dotnet:latest
 script:
 - pip install numpy
 - python main.py
```

## References
1. https://learn.microsoft.com/en-my/dotnet/core/install/linux-debian
2. https://docs.gitlab.com/ee/ci/docker/using_docker_images.html
3. https://medium.com/devops-with-valentine/how-to-build-a-docker-image-and-push-it-to-the-gitlab-container-registry-from-a-gitlab-ci-pipeline-acac0d1f26df
4. https://github.com/docker-library/ruby
