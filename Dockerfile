FROM python:latest

# Install dotnet 7 on Debian 11
RUN wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb && dpkg -i packages-microsoft-prod.deb && apt-get update
RUN apt-get install dotnet-sdk-7.0 -y

CMD ["bash"]